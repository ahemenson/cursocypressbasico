describe("Tickets", () => {
    beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"));

    it("fills all the text input fiends", () => {
      const firstName = "Ahemenson";
      const lastName = "Cavalcante";
      const email = "ahemenson@gmail.com";
      const request = "vegetarian";
      cy.get("#first-name").type(fistName);
      cy.get("#last-name").type(lastName);
      cy.get("#email").type(email);
      cy.get("#requests").type(request);
      cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("Alerts on invalid email", () => {
        cy.get("#email")
        .as("email") // Definindo alias (Apelidando um elemento)
        .type("ahemenson$gmail.com");

        cy.get("#email.invalid").should("exist");  // Valida exibição de alerta de valor inválido

        cy.get("@email") // recorrendo ao alias definido (@email)
        .clear()
        .type("ahemenson@gmail.com");

        cy.get("#email.invalid").should("not.exist"); // Valida não exibição de alerta de valor inválido
    });

    it("Select two tickets", () => {
      cy.get("#ticket-quantity").select("3");
    });

    it("Check 'Vip' ticket type", () => {
      cy.get("#vip").check();
    });

    it("Check 'Social Media' checkbox", () => {
      cy.get("#social-media").check();
    });

    it("Check 'Publication', 'Friend' checkboxes", () => {
      cy.get("#friend").check();
      cy.get("#publication").check();
      cy.get("#friend").uncheck();
    });

   it("Fills and reset the form", () => {
     const firstName = "Ahemenson";
     const lastName = "Cavalcante";
     const fullName = `${firstName} ${lastName}`;

     cy.get("#first-name").type(firstName);
     cy.get("#last-name").type(lastName);
     cy.get("#email").clear().type("ahemenson@gmail.com");
     cy.get("#ticket-quantity").select("2");
     cy.get("#vip").check();
     cy.get("#friend").check();
     cy.get("#requests").type("vegetarian");

     cy.get(".agreement p")
     .should("contain",
      `I, ${fullName}, wish to buy 2 VIP tickets.`);

     cy.get("#agree").click();
     cy.get("#signature").type(fullName);

     cy.get("button[type='submit']")
     .as("submitButton")
     .should("not.be.disabled");

     cy.get("button[type='reset']").click();

     cy.get("@submitButton").should("be.disabled");

   });

   it.only("Fills mandatory fields using support command", () => {
	   const customer = { // objeto customer com as chaves e valores
		   firstName: "João",
		   lastName: "Silva",
	     email: "joao.silva@email.com"
	   };

	   cy.fillMandatoryFields(customer); // Chamada da função

     cy.get("button[type='submit']")
     .as("submitButton")
     .should("not.be.disabled");

     cy.get("#agree").uncheck();
     cy.get("@submitButton").should("be.disabled");

   });

});
